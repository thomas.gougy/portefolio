// Utilities
import { defineStore } from 'pinia'

export const useAppStore = defineStore('app', {
  state: () => ({
    menu: false,
    aboutView: "experience",
    //
  }),
  actions: {
    toggle(){
      this.menu = !this.menu
    },
    setAboutView(value: string){
      this.aboutView = value
    }
  }
})
