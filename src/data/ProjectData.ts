export const ProjectData = [
    {
        name : "GanaLearn",
        img: "logoganalearn.png",
        presentation : "Projet réalisé pour au sein de l'entreprise Ganapati. C'est aussi le projet qui m'a permis de valider mon titre professionnel.",
        techno: ["Typescript","Angular", "Nestjs", "MongoDb", "Golang", "Git", "Bootstrap", "Docker", "Html", "CSS", "Jest"],
        gitLink : "",
        moreInformation : "C'est un projet qui m'a permis de toucher à la partie Front-end comme à la partie Back-end. Il a duré le temps de mon alternance. L'objectif était de développer un LMS/LRS afin de créer un support pour les formateurs indépendant suivi par Ganapati. Il a aussi servi pour le centre de formation pour la réalisation des cours à distance. L'application permettait avais pour but de pouvoir gérer les formations pour le CFA, la création d'évaluation et de contenu de cours pour les formateurs. Pour finir, la dernière partie était la partie étudiante dans laquelle il pouvait y retrouver des format PDF texte ou même des cours au format SCORM."
    },
    {
        name : "Portefolio",
        img: "https://www.vectorlogo.zone/logos/vuejs/vuejs-ar21.svg",
        presentation : "Il s'agit du projet sur lequel vous êtes actuellement. Il a été développé avec le framework Vuejs et Vuetify.",
        techno: ["Typescript","Vuejs", "Git", "Html", "CSS" ],
        gitLink : "https://gitlab.com/thomas.gougy/portefolio",
        moreInformation : "Mon portefolio que j'ai décidé de développer avec VueJs. Par la suite, je vais ajouter le parti Back-end avec symfony ce qui va me permettre de retoucher à du PHP et me former sur ce framework. C'est aussi grâce à ce projet que j'ai pu vraiment toucher à la partie dev-ops. Cela m'as permis de prendre mon premier VPS et déployé le front dessus avec un Nginx."
    },
    {
        name : "Light LMS Front",
        img: "certif.png",
        presentation : "Partie Front-end d'un projet réalisé au cours de ma formation, c'est à projet qui devait être réalisé sur 3 jours.",
        techno: ["Typescript","Vuejs", "Git", "Html", "CSS" ],
        gitLink : "https://gitlab.com/cloumic98/light_lms_back/-/tree/master?ref_type=heads",
        moreInformation : "Il s'agit ici de la partie Front-end d'un projet réalisé à deux avec une personne présente dans ma formation. Nous avons eu l'occasion de développer les deux projets en peer programming grâce a code with me proposer par la suite JetBrains. Ce projet était l'occasion de découvrir un nouveau framework il avait un but très simple créer une interface pour pouvoir avoir un visuel sur le résultat de nos requêtes. Nous avons donc simulé un centre de formation avec différents élèves et matières."
    },
    {
        name : "Light LMS Back",
        img: "certif.png",
        presentation : "Partie Back-end d'un projet réalisé au cours de ma formation, c'est à projet qui devait être réalisé sur 3 jours.",
        techno: ["Typescript","Node", "Git", "Express", "MongoDb"],
        gitLink : "https://gitlab.com/cloumic98/light_lms_front",
        moreInformation : "Il s'agit ici de la Partie Back-end du projet précedement présenté. Dans la partie Back-end, nous avons décidé de créer un query builder a la main dans une API express. Celle-ci fesait la liaison entre notre partie front et notre base de Données Mongo."
    },
    {
        name : "Projet Serveur GTA Roleplay",
        img: "pp-moonrock.png",
        presentation : "Projet en cours de réalisation, je le fais avec un plusieurs amis cela me permet aussi de découvir la gestion de projet et la prise de décision.",
        techno: ["Git", "Gestion de projet", 'LUA', 'OpenIV', "Paint.net"],
        gitLink : "",
        moreInformation : "Ce projet m'a permis de découvrir d'autres méthodes de fonctionnement, mais surtout de commencer à faire de la gestion de projet avec différentes ressources. J'ai pu découvrir et développer avec du LUA pour la création de divers fonction pour améliorer la base de jeux. J'ai aussi pu mettre les mains dans la création de modèles pour les différents besoins (véhicules, modification de la carte, charte graphique). C'est un projet que je vais réaliser sur le long terme qui vient tout juste de démarrer."
    },
    {
        name : "WattEcriture",
        img: "https://cdn-icons-png.flaticon.com/512/4103/4103001.png",
        presentation : "Tout premier projet que j'ai pu réaliser lors de mes premières semaines de cours.",
        techno: ["Git", "Php", 'Html', 'Css'],
        gitLink : "https://gitlab.com/thomas.gougy/wattpad",
        moreInformation :"Ce projet a été réalisé en PHP, il servait à mettre pour la première fois les mains dans les API. Le but était de faire une variante très simple du site Wattpad. Il devait y avoir deux API une publique et une privée pour les personnes authentifier avec un CRUD pour la création d'histoire."
    }
]