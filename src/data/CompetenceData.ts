export const CompetenceData = [
    {
        label: "Front-End",
        imgUrls: [
            "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/768px-Angular_full_color_logo.svg.png",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Vue.js_Logo_2.svg/2367px-Vue.js_Logo_2.svg.png",
        ], 
        message: "Sur mes divers Projet que ce soit en entreprise ou personnel j'ai eu l'occasion de travailler sur Angular et Vuejs."
    },
    {
        label: "Back-End",
        imgUrls: [
            "https://cdn.discordapp.com/attachments/611232179099271197/1182266663844982804/Alq28dL7m6khAAAAAElFTkSuQmCC.png?ex=658412a2&is=65719da2&hm=22bdde47abd2ccc1a0c06b17e05adae294c56c666db94b4d08b0ca03d1c3b219&",
        ],
        message: "Le Back-end est l'un des élément que j'ai le moins vu mais j'ai quand meme eu l'occasion de travailler sur NestJs. J'ai aussi eu l'occasion de voir un peux de Lua, PHP et Golang."
    },
    {
        label: "Base de Données",
        imgUrls: [
            "https://w7.pngwing.com/pngs/956/695/png-transparent-mongodb-original-wordmark-logo-icon-thumbnail.png",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/1200px-Postgresql_elephant.svg.png"
        ],
        message: "Lors de la création des différents projets, j'ai travaillé majoritairement avec Postgresql et Mongodb, mais j'ai aussi eu l'occasion d'utiliser mariaDb."
    },

    {
        label: "Dev-Ops",
        imgUrls: [
            "https://cdn.discordapp.com/attachments/611232179099271197/1182267683727745054/logomark-orange2x.png?ex=65841395&is=65719e95&hm=aca0b9bb8d14b79e6a57bf7cbe2e197e4bc4927c51c7f4302a0334af0f9d3ae5&",
            "https://cdn.discordapp.com/attachments/611232179099271197/1182267912090812516/docker-logo-6D6F987702-seeklogo.png?ex=658413cb&is=65719ecb&hm=dd4bc2aea1267af5addbb1b68adba58a8091745942c0793204b7a4cffdbc7ca8&"
        ],
        message: "Pour la partie Dev-Ops, J'ai pu pratiquer Docker et Git."
    },
]