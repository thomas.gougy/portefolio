export const FormationData = [
    {
        schoolName: "Ecole 404",
        schoolLogo: "404.png",
        label: "Concepteur Développeur d'Application",
        level: "Titre Professionnel de niveau VI",
        date: "Septembre 2021 / Juillet 2023"
    },
    {
        schoolName: "CIFAC de Caen",
        schoolLogo: "cifac.png",
        label: "CAP Pâtissier",
        level: "Certificat d'aptitude Professionnel",
        date: "Septembre 2011 / Aout 2013"
    },

]