export const ExperienceData = [
    {
        enterpriseName: "Ganapati",
        enterpriseLogo: "Ganapati.png",
        label: 'Apprenti Developpeur Fullstack',
        presentation: "Ganapati est une entreprise faite pour simplifier les démarches entre les formateurs et les organismes de formations. Cette entreprise s'est ensuite spécialisée dans le développement de logiciel. J'y ai rejoint l'équipe IT pour y développer Ganalearn avec l'aide de 2 autres alternants. ",
        date: "Septembre 2021 / Juillet 2023"
    },
    {
        enterpriseName: "Armatis LC",
        enterpriseLogo: "Logo20Armantis.png",
        label: 'Chargé de clientèle',
        presentation: "Centre d'appel dans lequel j'ai eu l'occasion de travailler en appel entrant pour les projets Carrefour et Engie. J'ai pu y apprendre l'écoute des besoins client. Ainsi que le traitement des dossiers et de la synthèse de ceux-ci quand cela était nécessaire.",
        date: "Juin 2019 / février 2021"
    },
    {
        enterpriseName: "Cause à Effet",
        enterpriseLogo: "cause.png",
        label: 'Recruteur de donateurs',
        presentation: "Entreprise qui travaille pour des grosses ONG pour envoyer des personnes chercher des donateurs dans la rue. J'ai participé à une Mission pour Sidaction.",
        date: "Mars 2018 / Mai 2018"
    },
    
    

    {
        enterpriseName: "La Boulaga",
        enterpriseLogo: "boulaga.png",
        label: 'Apprenti Pâtissier',
        presentation: "Première expérience professionnelle dans laquelle j'ai appris les bases du métier de pâtissier, c'est dans cette entreprise que j'ai réalisé mon alternance.",
        date: "Septembre 2011 / Aout 2013"
    }

]